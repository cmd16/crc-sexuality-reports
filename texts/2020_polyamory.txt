C. Polyamory

Sarah found herself attracted to her co-worker, Robert. At first she resisted her feelings and his rather obvious flirtations because she knew that
he was married to Jill. After a Christmas work party, however, she gave in
to her desires and started an ongoing, intimate relationship with Robert.
This went on for three months until Robert unexpectedly invited Sarah to
move in with him and his wife. Sarah's immediate reaction was to reject
the idea. But not wanting to break off her newfound relationship with
Robert, she gave him and polyamory a shot. She reasoned to herself: "I
love him and really believe that he loves me. If parents can love more than
one child, is it really crazy to believe that a great guy like Robert can love
me even while he still loves his wife? Plus, now our relationship will be in
the open, and I won't have to feel so guilty about hiding things from Jill."
Sam and Becky, both white, had been married for two years. Each grew
up in a Christian home and considered themselves to be Christians, even
though they only infrequently attended a local church. One of Sam's good
friends, Justin, who was single, started spending more and more time at
Sam and Becky's place and found himself increasingly attracted to his
friend's wife. Becky reacted positively to the advances of Justin but did
not want to begin a secret relationship with him behind her husband's
back. She and Justin instead shared their true feelings with Sam and
asked him to consider polyamory, in which Justin would be added to their
marriage relationship as a significant other. Sam ultimately agreed to this
proposal. When Christian friends of Sam and Becky learned about this
new relationship, they could not help voicing their concern and objections. Sam and Becky, however, pushed back, arguing that their situation
did not really qualify as adultery since everything was in the open and all
three persons involved agreed to this relationship. Further, they argued,
polygamy was practiced by many Old Testament heroes of the faith, and,
as far as they could tell, they couldn't find any explicit rejection of these
relationships in the New Testament.
Consensual nonmonogamy (CNM), or what is more commonly called
polyamory (from the Greek poly, meaning "many" and the Latin –amory,
meaning "love"), refers to a mutually agreed-upon sexual relationship between three or more people. (The first verifiable use of the word polyamory,
according to the Oxford English Dictionary, occurred in 1992.) Some of these
relationships are called "vees" (from the letter v), since they involve three
people, one of whom is involved sexually with the other two, while the other
two are not intimate with each other. This is slightly different from "triads,"
which involve three people, all of whom are sexually involved with each
other. Another type of these consensual nonmonogamous relationships is
a "quad," which involves four people, typically two couples who have an
intimate relationship with each other.
Within these different types of polyamorous relationships there are varying degrees of commitment and exclusivity. Some polyamorous relationships are explicit in identifying one sexual partner as "primary" and others
as "secondary." Other relationships do not make such distinctions among
their members. Most of these polyamorous relationships expect the different
members to be sexually active only with the members of their relationship.
Other relationships do not have this restriction. The biggest feature that all
polyamorous relationships have in common is a commitment to openness
and honesty. The partners insist on being open and honest with each other
about expectations and about the setting of ground rules with which everyone agrees.
Polyamory may sound strange and puzzling to many people. Nevertheless, polyamorous relationships are quickly becoming more common, not
only in the broader culture but even among some Christians. It is hard to
obtain definitive numbers because the phenomenon of polyamory is relatively new, and so far there have been few studies involving large sample
groups. Yet the studies that exist show a range of 3-5 percent of people in
North America currently living in a consensual nonmonogamous relationship. They suggest that 12-20 percent of Americans have been in some kind
of open sexual relationship at some point in the past. Sociologist Mark
Regnerus has claimed that 24 percent of people who identify themselves as
Christians believe that polyamorous relationships are morally permissible,
although he subsequently qualified this claim by saying that only 6 percent
of Evangelicals and 19 percent of Catholics consider polyamorous relationships to be acceptable.
A variety of different arguments have been forwarded by people who
identify themselves as Christians to defend the practice of polyamory. What
follows below is a survey and evaluation of the most commonly used of
these arguments.

1. Old Testament polygamy

		 The most common argument used to justify polyamorous relationships
involves an appeal to the numerous instances of polygamy in the Old
Testament. Many of the Old Testament heroes of faith had more than one
wife: Abraham, Jacob, Gideon, Saul, David, and Solomon. In fact, over
forty key individuals in the Old Testament were married to more than one
woman. Since so many persons in the Old Testament were in nonmonogamous relationships, it is claimed, similar consensual nonmonogamous
relationships should be permitted today.
		 There are two main responses to this argument. The first involves
distinguishing what is "descriptive" in the Bible—the report of something
that happened—from what is "prescriptive" in the Bible—the positive or
negative judgment about what happened. The fact that many Old Testament figures took multiple wives is descriptive. There is nothing in the
Bible, however, that indicates that polygamy is good and acceptable and
ought to be practiced. In other words, these examples are not prescriptive.
In fact, in the case of many Old Testament figures the Bible describes the
pain, division, and strife that emerged within these polygamous relationships, thereby implying significant disapproval.
		 The second and more important response to the appeal to Old Testament polygamy as a justification of modern polyamory is that the rest of
Scripture makes clear that God's intention is for sex to take place only in
a marriage relationship between one man and one woman. This divine
intent is revealed in the creation account of Genesis 1-2, in which God
provides Adam with one other person. It is confirmed in the New Testament in Jesus' double citation of this creation account: "'Haven't you
read,' he replied, 'that at the beginning the Creator "made them male and
female" [Gen. 1:27], and said, "For this reason a man will leave his father
and mother and be united to his wife, and the two will become one flesh"
[Gen. 2:24]?'" (Matt. 19:4-5). God's expectation that marriage be between
one man and one woman is assumed in Paul's exhortation to husbands
and wives in Ephesians 5:21-33. His extended discussion concerning marriage in 1 Corinthians 7 asserts that each man should have his own wife
(not wives) and each woman her own husband (not husbands).

2. Certain Old Testament laws

		 The second argument for polyamory is an extension of the first one.
Proponents of polyamory sometimes claim that the laws of Deuteronomy
17:17 and Deuteronomy 21:15-17 show that God accepted polygamy. In a
similar way, it is argued, God also accepts nonmonogamous relationships
like that of polyamory. Deuteronomy 17:17 requires that the king of Israel
"must not take many wives, or his heart will be led astray." This command seems to prohibit marrying an excessive amount of wives, but that
leaves open the possibility of marrying a few wives. Deuteronomy 21:1517 deals with the inheritance rights of the oldest son in a family in which
the father has sons from two different wives.
		 It is true that neither of these Old Testament laws prohibits polygamy,
but it is also true that neither one sanctions the practice. Such laws ought
to be viewed as the regulations of sinful practices that God tolerated due
to the hardness of human hearts. Here the parallel with divorce is helpful. Jesus makes clear that the Old Testament law permitting divorce did
not reflect God's will for the permanence of marriage but, rather, was a
divine concession: "Moses permitted you to divorce your wives because
your hearts were hard. But it was not this way from the beginning. I tell
you that anyone who divorces his wife, except for sexual immorality, and
marries another woman commits adultery" (Matt. 19:8-9).
		 Although God allowed divorce and regulated it through Old Testament
laws, that did not mean such divorce was in accord with his will. Divorce
was tolerated under the old covenant, but the new covenant reaffirms the
creational requirement of lifelong fidelity between a husband and wife. In
a similar way, although God tolerated polygamy and regulated it through
Old Testament laws, this was not his original intent for marriage; nor is it
in accord with his will.

3. Certain New Testament commands

		 There are a few occasions in the New Testament where the apostle Paul
commands that an elder must be "the husband of one wife" (1 Tim. 3:2;
Titus 1:6, RSV) and that a deacon must be the "the husband of one wife"
(1 Tim. 3:12). Some claim that, since Paul had to issue a special law against
polygamy among those in leadership positions, this implies that the Old
Testament toleration of polygamy remained in place in the New Testament church.
		 The key phrase in all these three texts, however, is capable of several
different meanings. The phrase, which literally reads "a man of one woman," can mean that an elder and deacon must either (1) be married rather
than single, (2) be monogamous rather than polygamous, or (3) be faithful
in his marriage. The first option is not likely, given Paul's strong commendation of singleness (1 Cor. 7:7-8; 7:38) as well as his own single state
(1 Cor. 9:1). The second option is also not likely. The exact same phrase
occurs in 1 Tim. 5:9 but refers to "a woman of one man." Here polygamy
cannot be in view, since there are no examples in the ancient world of a
woman having more than one husband. The meaning of the key phrase,
then, is most likely the third option—namely, the need to be faithful in
one's marriage.
		 Even if the phrase were a reference to polygamy, however, that would
not mean Paul intended to lay down a requirement that applies only to
church leaders and not to all believers.

4. Divine relationships

		 Another argument used to justify polyamory involves an appeal to
divine relationships, including (1) the relationship that the members of
the Trinity—Father, Son, and Holy Spirit—have with each other, and (2)
the relationship between God and his people. Chuck McKnight, who
self-identifies as a "progressive Christian," argues that "God is not in any
sense monogamous" and tries to prove this point by appealing to three
things: the Trinity, God's relationship with Israel, and Christ's relationship
with the church.
		 McKnight appeals to the Trinity as the most intimate and loving relationship imaginable, shared equally and eternally between three divine
persons. But although Christians ought to model the unity of the Trinity
(John 17), there is no indication in Scripture that the relationship among the
divine persons is intended as the paradigm for human sexual relationships.
		 McKnight also appeals to two Old Testament texts in which God is portrayed as having more than one wife. Jeremiah 3 describes God as being
married to both the southern kingdom of Judah and her sister, the northern kingdom of Israel. Ezekiel 23 portrays both Jerusalem and Samaria
as God's two adulterous wives. When these two texts are compared to
the testimony of Scripture as a whole, however, it is clear that Judah and
Israel (and their capital cities, Jerusalem and Samaria) are both part of the
one people of God. They were not supposed to be divided. Further, the
language of marriage and adultery in these texts is used metaphorically,
not literally. There is no justification for concluding, as McKnight does,
that these two texts "portray God as polygamous," and there is even less
justification for inferring from these texts that consensual nonmonogamous relationships mirror God's relationship with his people.
		 Finally, McKnight also appeals to the relationship between Christ and
the church as described in terms of a marriage relationship in Ephesians
5. He wonders how the marriage between Christ and the church ought to
be understood in the light of God's previous marriages, as he calls them,
to Israel, Judah, Jerusalem, and Samaria. He also stresses that although
the church is a single, corporate whole, "God has an intimate relationship
with each and every one of us. Christ's marriage to the church is ultimately a marriage to billions of individuals." This leads him to conclude
that "polyamory does at least provide a more-accurate picture of God's
relationships than monogamy."
		 The logic of this reasoning is extremely dubious. First, we are once again
dealing with a metaphor, so it would be dangerous to conclude too much
from this image about actual marriage relationships. Second, the metaphor
identifies the bride of Christ not with each individual believer but as the
collective body of believers. The bride is the church. Third, Paul quotes
Genesis 2:24 in the middle of this metaphor (Eph. 5:31), thereby stressing
that marriage is a relationship between one man and one woman.

5. "Born this way"

		 A more common argument used by proponents of polyamory appeals
to biology. This argument claims that some people have a sexual orientation that makes them predisposed against monogamy and gives them
an innate desire for sexual relationships with more than one person. For
people with such a polyamorous orientation, monogamy is unnatural.
Nonmonogamy is not just something that they do. It is who they are. The
argument that polyamory should be legally recognized as a sexual orientation has even appeared in a peer-reviewed law journal.
		 It is important to note that there is no evidence for the claim that polyamory is a biologically rooted sexual orientation. Yet even if it could be
proven that polyamory is a genetically caused sexual orientation, it would
not logically follow that consensual nonmonogamy is morally permissible
for Christians. As we noted in our discussion of homosexuality, people
are born with all kinds of biologically caused proclivities and desires that
Scripture identifies as sinful. As disciples of Jesus, we are called to overcome these inclinations through the power of the Holy Spirit.

