II. Pastoral Advice Re Homosexuality

In order that the churches may deal in a pastoral way with the problems of homosexuality we recommend that synod serve the churches
with the following statements of pastoral advice.
1. Homosexuality (male and female) is a condition of disordered
sexuality which reflects the brokenness of our sinful world and for which
the homosexual may himself bear only a minimal responsibility.
2. The homosexual may not, on the sole ground of his sexual disorder,
be denied community acceptance, and if he is a Christian he is to be
whole heartedly embraced by the church as a person for whom Christ
died.
3. Homosexualism as explicit homosexual practice must be condemned as incompatible with obedience to the will of God as revealed
in Holy Scripture.
4. The church must exercise the same patient understanding of and
compassion for the homosexual in his sins as for all other sinners. The
gospel of God's grace in Christ is to be proclaimed to him as the basis
of his forgiveness, the power of his renewal, and the source of his strength
to lead a sanctified life. As all Christians in their weaknesses, the homosexual must be admonished and encouraged not to allow himself to be
defeated by lapses in chastity, but rather, to repent and thereafter to
depend in fervent prayer upon the means of grace for power to with~
stand temptation.
5. In order to live a life of chastity in obedience to God's will the
homosexual needs the loving support and encouragement of the church.
The church should therefore so include him in its fellowship that he is
not tempted by rejection and loneliness to seek companionship in a "gay
world" whose immoral life-style is alien to a Christian.
6. Homosexuals, especially in their earlier years, should be encouraged
to seek such help as may effect their sexual reorientation and the church
should do everything in its power to help the homosexual overcome his
disorder. Members of the churches should understand that many homosexuals, who might otherwise seek therapeutic aid, are deterred from
doing so by the fear of detection and consequent ostracism. Christian
acceptance and support can in all such cases be a means toward healing
and wholeness. On the other hand, to those who cannot be healed and
who must accept the permanent limitations of their homosexuality, the
church must minister in the same spirit as it ministers to widows,
widowers, and the unmarried.
7. Christians who are homosexual in their orientation are like all
Christians called to discipleship and to the employment of their gifts in
the cause of the kingdom. They should recognize that their sexuality is
subordinate to their obligation to live in wholehearted surrender to
Christ.
By the same token, churches should recognize that their homosexual
members are fellow-servants of Christ who are to be given opportunity
to render within the offices and structures of the congregation the same
service that is expected from heterosexuals. The homosexual member
must not be supposed to have less the gift of self-control in the face of
sexual temptation than does the heterosexual. The relationship of love
and trust within the congregation should be such that in instances where
a member's sexual propensity does create a problem; the problem can
be dealt with in the same way as are problems caused by the limitations
and disorders of any other member.
8. It is the duty of pastors to be informed about the condition of
homosexuality and the particular problems of the homosexual in order
that the pastor may minister to his need and to the need of others, such
as parents, who may be intimately involved in the problems of homosexuality. The pastor is also in a position to instruct his congregation
in appropriate ways about homosexuality and to alert members and
office holders to the responsibility they bear toward homosexuals in the
fellowship. He can encourage an understanding of and compassion for
persons who live with this handicap, and dispel the prejudices under
which they suffer.
9. The church should promote good marriages and healthy family
life in which the relations between husband and wife and between
parents and children are such that the psychological causes that may
contribute to sexual inversion are reduced to a minimum. Parents should
be encouraged to seek Christian counsel ,and help when they see signs
of disordered sexual maturation in their children.
10. Institutions and agencies associated with the church that are in a
position to contribute to the alleviation of the problem of homosexuality
are encouraged to do so by assisting ministers to become better informed,
by offering counseling services to the homosexual and his family, and
by generally creating a Christian attitude in the churches as well as in
society as a whole.
11. The church should speak the Word of God prophetically to a
society and culture which glorifies sexuality and sexual gratification.
It should foster a wholesome appreciation of sex and expose and condemn the idolatrous sexualism and the current celebration of homosexualism promoted in literature, the theater, films, television, advertisements, and the like.