A. Singleness

As part of our mandate, we have been asked to "provide concise yet clear
ethical guidance for what constitutes a holy and healthy Christian sexual life,
and in light of this to serve the church with pastoral, ecclesial, and missional
guidance that explains how the gospel provides redemptive affirmation
and hope for those experiencing sexual questioning, temptation, and sin."
In preceding sections of this report we have summarized biblical teaching
on sexuality and addressed the issues of pornography, gender identity, and
homosexuality in substantial detail. In this section we offer some reflections
on other matters of concern to the church: sex and singleness, polyamory,
and divorce. We are aware that our study and discussion of these matters is
less thorough than with those other issues. However, because these additional matters are so important to a balanced assessment of sexuality and the
Christian life, we believe it is important to offer at least some guidance in
these areas.
According to the 2017 report of the U. S. Census Bureau, about 50 percent
of all American adults are unmarried. Millennials are the most likely to be
single, with close to 59 percent of millennials unmarried. This is partly the
result of young people delaying the age at which they marry. The average age of marriage for a male in 2017 was 30, and for women it was 28, an
increase of about five years for men and six years for women since 1980. But
being single is not necessarily a factor of age. About 45 percent of persons 65
and older are single.
For a large part of church history, especially the first few centuries, unmarried persons were held in high honor. To encourage and validate singleness,
leading church figures such as Ambrose, Gregory of Nyssa, and John Chrysostom invoked Jesus' words about marriage after the resurrection (Matt. 22:2932), his praise for people who become eunuchs for the sake of the kingdom
(Matt. 19:12), and Paul's encouragement and validation of singleness (1 Cor.
7:25-35). Drawing specifically on 1 Corinthians 7, the early church recognized
that people who are married will have divided interests with respect to the
Lord. By contrast, single people are less restricted by "the affairs of this world"
and are free to pursue "the Lord's affairs" (1 Cor. 7:32-34).
Endorsing celibacy (the Latin word caelibatus literally means "unmarried state" or "singleness") in the early centuries of the church was radically
countercultural. Marriage was not considered optional. Young men and
women were expected to do their duties to their families and to society by
marrying young, raising children, and establishing a prosperous household.
For women in particular, life often consisted of marriage during a girl's
teenage years, a decade or two of producing many children, and often death
in childbirth. Whereas many Christians today experience singleness as a
burden, many in the early church experienced it as liberation. Women and
men who did not marry were free from a plethora of social constraints to
pursue the cause of the kingdom of God. They encouraged it as "an entire
way of life . . . that is open to any who seek it."
Sexual desire is a healthy and normal part of being human. Scripture
teaches that we have been created for relationship with God and other
humans. One mark of this capacity for relationship is the desire for intimacy
with others. Sexual intimacy is just one form of this. The Oxford English
online dictionary defines intimacy as "the state of having a close personal
relationship with someone." Intimacy is present in many different types
of relationships, including among friends and between parents and their
children, sisters and brothers, and a husband and wife. On the other hand,
although all of these relationships may involve intimacy, intimacy is not
necessarily a component of any of them.
Just as emotional intimacy need not include sexual intimacy, so sexual
intimacy is often present without emotional intimacy. In fact, the irony of our
time is that despite an amazing level of connectedness, particularly through
social media, and despite widespread permissive attitudes toward sexuality, loneliness is reported to be at epidemic levels. One recent study even
claims that "loneliness is a prevalent and urgent public health issue." In
2018 Prime Minister Theresa May of the United Kingdom declared loneliness to be one of the great health issues of our time, even going so far as to
appoint a Minister of Loneliness to help address the problem.
Many single adults are lonely. At the heart of this loneliness is not simply
a desire for casual friendship, a handshake on Sunday morning, or participation in a "singles group" at church. Rather, singles, like all people, desire
deep and lasting intimacy with other humans. Wesley Hill, a celibate gay
Christian, writes,
The love of God is better than any human love. Yes, that's true, but that doesn't
change the fact that I feel – in the deepest parts of who I am – that I am wired
for human love. . . And the longing isn't mainly for sex (since sex with a
woman seems impossible at this point); it is mainly for the day-to-day, small
kind of intimacy where you wake up next to a person you pledged your life
to, and then you brush your teeth together, you read a book in the same room
without necessarily talking to each other, you share each other's small joys and
heartaches.
Hill notes that as human beings we desperately need "love, affection,
companionship, permanent intimacy, life-giving community, a deep sense
of belonging, a safe haven, a home." Unfulfilled, these longings are like a
gaping wound and terribly painful.
Part of the problem, Hill argues, is that the church has come to emphasize
marriage and family rather than friendship as the most significant relationships in people's lives. Single people's need for intimacy is almost completely ignored. Further, even many married people are lonely. While there
is nothing wrong with celebrating marriages, families, and all that goes with
them, the church also needs to find ways to foster friendship and intimacy
outside of marriage and family. Hill himself has written extensively about
what he calls "spiritual friendship."
Hill's point is well taken. All too often our friendships are limited to the
members of our families or to people with whom we have common interests
and hobbies. They exclude people who are not our age, gender, or socioeconomic status. Yet it is through friendship that we most often find genuine
intimacy with one another. Friendship leads us to take responsibility for one
another, bearing each other's burdens and encouraging, exhorting, challenging, and rebuking one another to seek what is good. If, as Christians, we are
not ultimately created for marriage but for friendship in the family of God,
how is it that we have come to emphasize marriage and family more than
the friendship to which all of us are called?
According to Christian teaching, to be an unmarried follower of Jesus is to
be called to celibacy regardless of age. Sexual relationships outside of marriage, whether casual or committed, are immoral. Celibate singleness is often
difficult; it can be very difficult to accept and is often a call that materializes
over time. Kathryn Wehr observes that over a period of years a person might
gradually move from rejecting the call, to reluctantly accepting it, to accepting it with joy. It should be remembered that for many people the call to
lifelong marriage is difficult as well, albeit in different ways.
There are a number of things the church must do to help single people
who are committed to honoring God with their bodies. We should recognize that our overemphasis on marriage and family has been hurtful and
exclusive. We can focus not just on creation but also on resurrection, as Mary
Hulst puts it, "rejecting the Gnostic notion that we can do what we want
with our bodies because they are dust, and instead embrace that we will do
what God wants with our bodies because they are his." We can encourage
singles to base their sexuality not on a potential future mate, but on their
"present relationship to God." In addition, we can preach about sex and
include in our pastoral prayers those who are struggling with loneliness
and sexual temptations of various kinds. We can preach about celibacy and
singleness, holding up Jesus and the apostle Paul as examples for Christians
to follow. We can welcome those who struggle and fail by making clear that
grace happens in the church. We can say to the singles in our church, "We
see you, single people, and we know how challenging it is to follow Jesus in
your sexuality, and we know that our emphasis on families and the idolatry
of marriage has hurt you. Your singleness matters to God, and it matters to
your church."
One way the church can offer helpful teaching in the area of sex and
singleness is to approach it as a matter of virtue. Virtue ethics focus on habits
that enable a person to flourish in accord with his or her created purpose.
The emphasis does not fall on rules but on practices and dispositions. The
virtuous life is characterized by struggle, for it takes discipline to put vices to
death and to turn new practices into virtuous habits. But this struggle to stay
morally on target can be, as one author puts it "a sign of health—the growing pains of character." When the new practices do become habitual, they
bring joy. They enable a person to flourish in a way that honors the creator.
In the area of sex and singleness the most important virtue is that of chastity. To practice chastity is to live out one's sexuality in a way that conforms
to God's created purpose for human beings as male and female, whether
married or single. Practicing chastity is characterized by seeking the flourishing of the persons with whom we are in relationships, the health of these
relationships, and the honor and glory of God, who has created us for such
relationships. As DeYoung puts it, "Proper use and enjoyment of our sexual
nature should track the way sexual desire and its fulfillment can enhance our
relationships with God and each other." This leads us to refuse to objectify
other people or treat them as means to the end of our own sexual satisfaction. Simon notes: "Chastity, as a virtue, is not just the ability to 'do without
sex' for weeks or months. More importantly, it keeps our sexual desires from
making us view others as collections of sexually arousing body parts."
The vice of lust, in contrast to chastity, makes sex primarily about me and
my pleasure, rather than about God and my neighbor. "In lust, my own pleasure is the goal, and I decide where to get it, and when, and with whom,"
DeYoung writes. Lust is powerful because it is deceptive. It feels right. We
often confuse it for love. Yet it is never loving to enter into a sexual relationship that is outside of the will of God. Such a relationship can never lead
to genuine flourishing, even if for a time it feels like it does. Thus unchaste
sexual activity—sexual activity that exhibits the vice of lust—includes such
things as any sexual behavior prohibited by Scripture, living together in a
sexual relationship outside the covenant commitment of marriage, having
sexual partners outside of the marriage covenant, use of any form of pornography, prostitution, going to sexually charged environments like strip clubs,
fantasizing about sex with persons to whom one is not married, and the list
goes on. "Lust is always in pursuit and ends as empty-handed as it began."
It does not ultimately satisfy, and it often causes tremendous harm by
alienating a person from God or from other people, destroying relationships,
even at times provoking a person to commit sexual violence or other abusive
behavior. It can also lead to sexually transmitted diseases and infections, as
well as psychological, emotional, and spiritual harm.
The virtue of chastity involves both married and single persons. Sex, as
God designed it, is both psychologically and biologically good and purposeful. The penultimate ends of sex are, first, bonding two people into a
one-flesh union, and, second, creating new life, something symbolized in the
birth of children, in whom what God has joined together literally cannot be
separated. In other words, sex is potentially both unitive and procreative.
The effects of original sin as well as our own actual sin can thwart those
potentials. Though marriage is not a guarantee of perfect intimacy, sexual or
relational, sex in its proper context and directed toward its intended purpose
is a wonderful gift.
Given that our society emphasizes sexual intimacy as the truest form of
intimacy, it is no wonder that so many people feel that they have to have sex
in order to have intimacy. But genuine intimacy need not take sexual expression. As a church, we need to reflect much more on how we can encourage
and support one another to establish deeper friendships. Single and married people alike should be taught to invest in friendships with one another
that are both chaste and intimate. Christians who are mature and who have
mature friendships with other Christians should be held up as examples
from whom the rest of us can learn. God has called us his friends. We need to
learn to be friends with one another.

