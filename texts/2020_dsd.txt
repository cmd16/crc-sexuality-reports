Appendix B: Disorders of Sexual Development and Their Implications

I. Introduction and background

"Standard" (or "normal") sex development in both females and males
is usually taken to mean the alignment of certain biological elements in
a developmental sequence, from conception to birth, through childhood,
a­dolescence, and then adulthood. A certain pattern of sex chromosomes
(XX or XY) at conception leads to a differing balance of hormones (e.g.,
estrogen, progesterone, and testosterone), a differing pattern of internal sex
organs (e.g., ovaries, uterus, and fallopian tubes in females; testicles, seminal
vesicles, and prostate gland in males), and at birth a set of differing external
genitals (e.g., clitoris, labia, and vagina in females; penis and scrotal sac in
males). When childhood merges into puberty, a new surge of hormones—
again in different ratios for girls and boys—leads to differing secondary sex
characteristics (e.g., breasts in girls, beards in boys) and to the possibility
of parenthood, as ova mature on a cyclic basis in girls, and boys begin to
produce sperm.
Of course, this summary of sex development masks the sheer complexity
of the pre- and postnatal processes that take place from conception to standard adult reproductive maturity. To understand various disorders of sex
development and their medical, psychological, legal, and pastoral implications, it helps to know in more detail the process of standard reproductive
development. It is when that course of development takes a different turn—
for one reason or another, at one developmental stage or another—that a
disorder of sex development results.
Both a mother's and father's gametes (ova and sperm) are needed to conceive a child. The mother's ovum—always formed from an X chromosome—
combines with a sperm from the father that is formed from either an X or a Y
chromosome. If two X-chromosome gametes (one from each parent) unite,
the eventual child is usually an anatomically and physiologically normal
girl. If an X and a Y combine (from mother and father respectively), the child
is usually a normal boy. A father's gametes are thus responsible for the sex
of the child: a father's X plus a mother's X results in a girl; a father's Y plus a
mother's X results in a boy.
The developmental processes by which this happens are complex and not
neatly separable along parallel "X-directed" or "Y-directed" paths in utero.
Right from conception, normal male reproductive development requires
something more than just a Y chromosome: it requires vital cooperation from
a range of genes on the X chromosome. In addition, reproductive development in both sexes also needs the help of many genes not located on the
sex chromosomes at all, but rather on various nonsex (or "autosomal")
chromosomes.
However, none of this begins to happen until toward the second trimester
of pregnancy because, for the first several weeks, male and female embryos
share a single, undifferentiated developmental platform. Indeed, if we were
to examine a tiny fetus that miscarried around the ninth week of pregnancy,
we could not tell by examining either its external or internal structures
whether it was female or male.
Externally, we would see only an undifferentiated "urethral groove" and a
bump called the "genital tubercle" that is neither penis nor clitoris. Internally, we would find neither testes nor ovaries. Instead, attached to the indifferent gonads would be two sets of tubes—one looking like a tiny set of combs
(the Wolffian ducts), the other like a pair of slender trumpets (the Mullerian
ducts). Wolffian ducts are the precursors of male internal reproductive
structures, such as the prostate gland and the seminal vesicles. Mullerian
ducts are the precursors of female internal reproductive structures, such as
the uterus and fallopian tubes. But the normally developing fetus with XX or
XY chromosomes comes preloaded, so to speak, with both. And that is why
almost all of us have vestigial remnants of one or the other on our gonads
even now.
It is at this point that standard male/female sexual differentiation begins
to take place, continuing in a step-wise (or we could also say "domino")
fashion until birth. In other words, certain processes need to occur at the
right time and in the right order for a standard newborn girl or boy to result.
But these processes do not occur in a completely symmetrical fashion for
boys and girls. Biological research seems to show that, for humans as well as
other mammals, the fertilized zygote's default setting is to produce a female.
In other words, male development requires certain additions along the way
if it is not to default toward the female direction.
The first of these asymmetries occurs in those early, bean-shaped "indifferent gonads." If they are to become testes, an additional substance (known
as H-Y antigen, which is controlled by genes on the Y chromosome) needs to
kick in. In the rare instances where this does not occur, the indifferent gonads
will become ovaries, regardless of the XY sex-chromosomal makeup of the
fetus. Moreover, there does not seem to be an analogous antigen needed for
the indifferent gonads in an XX fetus to begin developing as ovaries: around
the eighth week, they just begin to do so without any (so far known) additional input.
Only about a week later, a second asymmetry occurs when the tiny
­Wolffian or Mullerian ducts are ready to start developing, respectively,
as male or female internal sexual anatomy. With the help of another bio­
chemical substance (called MIS, or Mullerian-Inhibiting Substance), the
comb-like ­Wolffian ducts in the male fetus start differentiating into structures like the prostate gland and seminal vesicles, while the accompanying
Mullerian ducts (the precursors of internal female development) regress
until they become vestigial remnants on top of the now-developing testes. In
female ­fetuses the male-leaning Wolffian ducts regress and become vestigial
remnants on the now-developing ovaries, apparently without any added
biochemical help, while the uterus and fallopian tubes also begin to form.
If these standard processes occur without interference, the now-­
differentiated ovaries and testes will begin to produce sex hormones, one of
whose main jobs (from about the ninth week) is to turn the undifferentiated
external genital area of the male fetus into a penis and scrotum. This too is
a complex process. It is misleading to refer to these reproductive hormones
(which include testosterone, estrogen, and progesterone) as "female" or
"male," since all of them are normally produced, though in different ratios, by both ovaries and testes—and in the case of testosterone, in smaller
amounts by the adrenal glands. In optimal amounts, testosterone is crucial
at this stage for the formation of the external genitals and the prostate gland
in the male fetus—but also for egg development in the ovaries of the female,
showing again how interwoven are the processes of girls' and boys' prenatal
sex development.
At this stage, a third asymmetry between male and female development
occurs. Testosterone—normally produced in greater quantities by a male
than a female fetus—is needed to masculinize a boy's originally undifferentiated external genitals. But to complete the process, a kind of "super
t­estosterone"—called DHT, or dihydrotestosterone—must also be added.
DHT is formed when a certain enzyme acts on ordinary testosterone, and in
very rare cases, when DHT is absent, the fetal male's penis is apt to be small
and also to lack the seam underneath it that normally encloses the earlier,
open urethral groove into the scrotal sac.
If the above-described fetal developmental processes proceed in the usual
male or female direction, they will result in standard male or female internal/external anatomy at birth, and in the physiological potential for fertility in adulthood. Knowing about these processes in somewhat more detail
should now make it easier to understand how they may depart from the
norm to become various disorders of sex development. It is customary to divide these conditions between those that result from 'chromosomal anomalies,' which occur before the zygote even becomes a developing embryo, and
those that result from "hormonal anomalies," which occur in the postconception stages of development.
More will be said below about the frequencies of various disorders of sex
development, but overall, some estimates suggest that a disorder of sex development of some sort may be found in as many as one in a hundred people
(see Table 1). Estimates vary, depending on the populations sampled and the
sampling methods used. Either way, these conditions are frequent enough
that it is likely almost all of us know someone who has one, even if we have
not been told about it.

II. Sex chromosomal anomalies

A sex chromosomal anomaly is a condition in which the affected person
is conceived with other than the standard number of sex chromosomes for a
female (XX) or male (XY). This can also happen with nonsex chromosomes:
most of us, for example, know about Down Syndrome, or Trisomy 21, where
an individual has three instead of the standard two copies of chromosome 21.

A. Turner Syndrome (Monosomy X, or X0)

Earlier we mentioned that a zygote with a single Y (but no X) chromosome cannot survive in utero beyond that stage, but that the opposite
case—persons who have only a single X (but no accompanying X or Y)c­hromosomes are not uncommon. Known as Turner Syndrome, it is estimated to occur in 1:2,000 or 3,000 births. Its fetal developmental journey is
largely that of a standard female in terms of both internal and external anatomy—except that the ovaries are underdeveloped, often to the point of being
just streaks of undifferentiated tissue. Streak ovaries usually cannot produce
sufficient amounts of hormones like estrogen and progesterone needed for
later breast development, fertility, and menstrual cycle regulation. Children
with Turner Syndrome tend to be short in stature and to have broader than
average chests and a weblike neck, but are otherwise not noticeably different
from their female peers. They are, however, at risk for heart defects, thyroid
disease, diabetes, and/or other immune disorders, and a small percentage
exhibit signs of mild cognitive retardation.
Until the advent of hormone replacement therapy, Turner syndrome children were unlikely to undergo puberty, and thus were not likely to menstruate or develop breasts. Estrogen replacement therapy has changed this, and it
is even possible for some Turner persons to gestate a child via egg donation.
The possibility of such interventions is usually welcomed, as most Turner
persons have a quite stable female gender identity. Is a Turner person, with
only single-X chromosomes, someone who should be routinely expected to
undergo estrogen replacement therapy? After all, the original missing chromosome was just as likely to have been a Y as an X, so perhaps testosterone
therapy beginning in adolescence should be a routine possibility for Turner
persons who might prefer to look and function more like a male. Or what if
no hormone intervention at all is preferred, no matter what the result might
be for adult bodily appearance?

B. Metafemale Syndrome (Trisomy X, or XXX)

At the other extreme from X0 Turner persons are women who have an extra X chromosome—hence the designation XXX, or Trisomy X (analogous to
Trisomy 19 for Down Syndrome persons). More rarely, this syndrome can appear as XXXX, or even XXXXX. Its frequency is estimated to be about 1:1,000
persons and (like Down Syndrome) is more likely to occur in the children of
older mothers. Women with Trisomy X (or its variations) tend to have longer
legs and more slender torsos, but otherwise present as standard females in
terms of internal and external sexual anatomy, secondary sex characteristics,
fertility, and childbearing potential. Historically, before the advent of microbiology, these woman would not have been identified as out of the ordinary
at any stage of their life, though they did (and do) have a greater than ordinary chance of premature ovarian failure.

C. Jacob's Syndrome (XYY or Metamales)

As females can be conceived with one or more extra X chromosomes, so
can males be conceived with an extra Y chromosome (XYY) or, more rarely,
two (XYYY). The frequency of XYY males is estimated at between 1:1,000 and
1:2,000, and, like females with extra X chromosomes, there is little to distinguish them from standard chromosomal males, other that the likelihood of
being taller than average, and sometimes having mild cognitive developmental delays. The same is not true for (the very rare) XYYY males, who are
more likely to have skeletal abnormalities, delayed bodily development, and
much lower intelligence scores than either multi-X females or males who
have only one or one extra Y chromosome.
Since mandated chromosome checks of newborns are usually just the ones
needed to identify intersex syndromes with serious health implications (of
which XYY is not one), a large percentage of XYY males live ordinary lives
not even knowing about their extra Y.

D. Klinefelter Syndrome (XXY)

Klinefelter Syndrome males have an extra X chromosome (or, much more
rarely, an extra two or three). The classic XXY version is one of the more
common sex chromosomal anomalies, with a frequency of about 1:500. Like
XYY males, XXY males are not usually identified by a chromosomal check in
the delivery room, and so may go though childhood or even beyond without being detected. Their external appearance at birth is standardly male,
because although their extra X chromosome results in a lowered level of fetal
testosterone, it is not usually low enough to visibly affect external genital
appearance.
However, lower testosterone production also occurs prior to puberty,
with the result that secondary sex characteristic development is slow or
incomplete. Klinefelter adolescents may have less developed musculature,
less facial and bodily hair, weaker bones and somewhat broader hips. They
may also develop breast tissue (a condition known as gynecomastia). By
adulthood their appearance is similar enough to fit within the standard male
range, though they tend to be above average in height, and to have a lanky
build and youthful facial appearance, or alternately, a rounded body with
some continuing gynecomastia. They also have reduced (or sometimes no)
sperm production, and it may only be if they are unsuccessful in fathering
children that their XXY condition is detected.
Klinefelter syndrome is not life threatening, though it does increase the
risk of health problems more typical of women, such as breast cancer, osteoporosis, and autoimmune disorders. In terms of gender identity, most people
with Klinefelter syndrome identify as male and are less apt to be homosexual
than somewhat asexual. Many of their physical challenges can be alleviated
by testosterone replacement therapy. Their biggest challenge may be gaining acceptance by others despite having less than stereotypically masculine
appearance and interests.

E. XX Males (de la Chapelle Syndrome)

Strictly speaking, the last two syndromes to be discussed are not sex
chromosomal anomalies in the sense that they do not involve nonstandard
numbers of sex chromosomes. But because they can be clinically mistaken
for other sex chromosomal or hormonal anomalies, it seems appropriate to
deal with them here.
From the earlier discussion about standard male development, you may
recall that a gene on the Y chromosome, called the SRY (or sex-determining
region of the Y) is a necessary—but not sufficient—condition to produce a
standard male child at birth. But in rare instances (about 1:20,000), this area
of the Y chromosome breaks off during meiosis (when XY cells divide to
form the two types of sperm in the father's testes) and attaches to its neighboring X-chromosome instead. If the SRY-added X chromosome from the
father fertilizes a standard X chromosomal egg from the mother, the result
will be what is called an XX (or de la Chapelle syndrome) male.De la Chapelle, or XX, males are born with a penis and testes but no internal female reproductive structures, such as ovaries and uterus. However,
their testes are sterile and sometimes remain undescended after birth, and
the urethra sometimes appears on the underside—rather than on the tip—
of the penis (a condition known as hypospadias). Yet most de la Chapelle
children have the external appearance of a standard male, and almost always
grow up with a male gender identity. The condition is often not diagnosed
until after puberty, when the smaller size of their testes, their failure to produce sperm, and the development of breast tissue become causes for concern. Sometimes, in the absence of further tests, they may be misdiagnosed
as XXY (Klinefelter) males.

F. XY Females (Slyer Syndrome, or XY Gonadal Dysgenesis)

Swyer syndrome is more or less the opposite of de la Chapelle's syndrome, in that it also involves problems with the Y chromosome's SRY
gene—not when that gene is transferred to the X chromosome, but rather
when it fails to function on its own Y chromosome due to certain genetic mutations. It is rarer than de la Chapelle's syndrome, with a frequency of about
1:80,000. Swyer fetuses, despite their XY chromosomal makeup, illustrate the
principle that, in the absence of a functional SRY gene, the embryo will travel
along the female path, developing a uterus, fallopian tubes, cervix, vagina,
and standard female external genitalia at birth. However, Swyer syndrome
gonads are neither ovaries nor testes, but merely "streak gonads" which can
produce neither gametes nor sex hormones of any kind.
Swyer newborns are rarely diagnosed as such at birth, and are raised as
girls, typically with female gender identification. When menstruation and female secondary sex characteristics (other than pubic hair, thanks to the adrenal testosterone) fail to appear, an initial diagnosis of (X0) Turner syndrome,
or AIS (androgen insensitivity syndrome, which is discussed below) may be
considered. But when karotyping reveals an XY chromosomal pattern, and
internal body imaging tests confirm the presence of streak gonads, a diagnosis of Swyer syndrome may be made. As with most intersex conditions, this
syndrome is not life threatening. But because the streak gonads are at some
risk of becoming cancerous, they are often surgically removed.
As in Turner syndrome, both menstruation and breast development can
be induced in these XY females with estrogen and progesterone replacement
therapy, and some may become pregnant with a donor egg or embryo.

III. Sex hormonal anomalies

Sex hormonal anomalies do not involve nonstandard numbers of sex
chromosomes but occur in persons conceived with the usual XY male or
XX female sex chromosomal pattern. They begin instead when something
hormonally unusual happens during postembryonic fetal development. In a
sense, there are both fewer and more of them compared to sex chromosomal
anomalies. In their classic forms, there are three syndromes—but all three
come in various gradations, and thus are divided into what are called "partial" and "complete" types. Indeed, part of the problem of making accurate
frequency estimates of these syndromes may stem from ongoing discussions about how (or if at all) to subdivide the "partial" types into more and
less s­evere forms. Moreover, of all the disorders of sex development (both
c­hromosomal and hormonal), these three are probably the most heavily
"­politicized" in discussions of sex and gender.

A. Androgen Insensitivity Syndrome (AIS) in XY fetuses

Intersex persons with Androgen Insensitivity Syndrome have an XY sex
chromosomal pattern but (in its complete form) are born with standard female external genitals at birth. However, their internal reproductive anatomy,
including gonads, is that of a standard male. They are almost always raised
as female, have a strong female gender identity, and in adolescence undergo
normal-range female breast development. Complete AIS (CAIS) persons,
who identify as female (as almost all do) often marry and have "normal
heterosexual" relations with their (standard XY) husbands. Because there are
few health risks to this syndrome, sex chromosome karotyping at birth is not
always mandated, so CAIS infants may nonetheless leave the delivery room
having been recorded as ordinary XY males. The combined frequency of CAIS
and Partial AIS (PAIS) births is estimated at about 1 in 20,000.
How does AIS come about? Testosterone in optimal amounts is needed
in an XY male fetus to complete the conversion of the early, undifferentiated
genital area into a standard penis and scrotal sac after the testes, prostate,
and other internal male structures have been laid down. But in a CAIS fetus,
there is a mutation on the X chromosome that makes the fetal body unable
to use any of the testosterone being produced by the testes or (in smaller
amounts) by the adrenal glands. So normal-range testosterone is being produced, but it is functionally unusable. This means that the rest of the fetus's
reproductive development defaults to the female direction, and the penis
and scrotal sac do not form.
We might well ask why AIS persons develop breasts at puberty, and this
is where another complexity of hormonal ratios needs to be explained. We
know that both ovaries and testes—though normally in differing ratios—
produce the complete range of reproductive hormones, including testosterone, estrogen, and progesterone. This is the case both in fetal development,
and later again at puberty. Because AIS is a lifelong condition, and not
just limited to fetal development, the adolescent CAIS person is producing testosterone in the amounts needed to bring about male secondary sex
characteristics—but again, the body cannot use any of it, so the voice does
not deepen, nor does a beard grow or other bodily hair thicken. Instead, the
smaller amounts of progesterone and estrogen (which are not affected by the
original genetic mutation, and so can be taken up by the body's cells) get to
work and do what they do in standard XX adolescent females: they produce
breast tissue.
What they do not do (as you may have already guessed) is produce a
menstrual cycle, which cannot occur in the absence of a uterus. So it is in
adolescence that CAIS may be identified, if a diagnosis has not been previously made. Medically speaking, the only thing to be concerned about are
the still-undescended testes, which are at some risk for cancer, and thus are
usually removed.
So far we have been discussing only Compete Androgen Insensitivity
Syndrome (CAIS). In its less common, "partial" form (PAIS), for reasons
still largely unknown the original X-chromosome genetic mutation is only
partially operative. This means that functional fetal testosterone levels are
reduced, but not to a zero level of effectiveness. The result at birth is a varying level of external genital ambiguity—from not quite standard male, to
not quite standard female. This of course means that the requisite chromosomal and internal scanning tests for AIS will be done, and a diagnosis made
shortly after birth. But that can also lead to difficult decisions.
In the centuries before cosmetic surgery became safer and more technically sophisticated, persons born with ambiguous external genitals simply
grew up with them. Now it is possible to surgically "feminize" the ambiguous genitals of a PAIS infant to look like those of a more standard female. But
should it be done if the parents request it? Should it wait until the child is old
enough to state a preference? Moreover, such genital surgery in infancy is a
delicate business, and can result in (often seriously) reduced sexual arousability in adulthood, if a penis-like clitoris has been surgically reduced for
reasons of appearance. There are many adults who, having had such surgery when they were too young to consent to it, are vocally angry about the
problems—not life threatening, but still serious—that have resulted, including not just reduced genital sensitivity, but also the development of painful
scar tissue. It is becoming more common in medical circles to recommend
delaying surgery (if any) to well beyond infancy, then raising a PAIS child to
have a (provisional) gender identity as either male or female, and making a
decision about surgery on a case-by-case basis, with various people—including parents, the child, medical, and often psychological personnel involved.

B. Congenital Adrenal Hyperplasia (CAH) in XX fetuses

In a rough sense, this hormonal anomaly is the reverse of the one just
discussed, in that in its extreme form (Complete CAH) a fetus with XX or
standard female chromosomes is born with standard male external genitalia.
In its less extreme form (Partial CAH), the external genitals may be ambiguous to varying degrees, just as they are in Partial AIS births—and this leads
to the same challenging decisions regarding genital surgery in infancy and/
or how to raise the child initially in terms of gender assignment.
The mechanism behind Congenital Adrenal Hyperplasia, however, is
quite different, though its awkward name summarizes what it is: congenital
means that the syndrome occurs before birth and continues beyond; adrenal
means that the adrenal glands are involved; and hyperplasia means that those
adrenal glands are overproducing something. In this case it is testosterone,
which is usually only produced in minute amounts in standard female and
male adrenals. But in CAH, due to the mutation of a recessive gene on one
of the nonsex (autosomal) chromosomes, the adrenal glands overproduce
testosterone greatly. Because it is a recessive gene, both the child's mother
and father must have it in order for CAH to occur. When this happens in a
XX fetus (whose gonads and internal reproductive structures have already
differentiated as female), the remaining external genital development will be
partially or completely male.
CAH can occur in either a female or a male fetus, and it occurs in about
1 in 15,000 births. It is also involves a serious health risk in that it is accompanied by salt imbalances that, without appropriate medical treatment,
can cause bodily dehydration and death. For this reason, it is one of the
conditions usually tested for among newborns in hospital delivery settings—although even today, about 25 percent of Complete CAH cases are not
diagnosed until later. In boys it has no effect on external genital appearance,
but it can lead to premature puberty (with a growth spurt and secondary
sex characteristics) as early as about age seven. And the same can happen in
girls: pubertal development is too early and too typically male—all this in
addition to the partial or complete masculinization of external genitals that is
present at birth.
Medically, the solution for both the risky salt imbalances and the overproduction of adrenal testosterone is to prescribe various forms of cortisone (continuously) and other medications (temporarily) to slow down the
premature pubertal growth spurt. Girls with CAH most often have a female
gender identity and if successfully treated medically will be fertile and able
to bear children.

C. 5-Alpha Reductase Deficiency Syndrome (5-ARD) in XY fetuses

One of the hormones needed to complete external genital formation
(i.e., the closing of the underseam of the penis and scrotal sac) is a kind of
"super testosterone" known as dihydrotestosterone, or DHT. This hormone
is formed when an enzyme known as 5-Alpha Reductase acts on ordinary
testosterone. But in very rare instances this does not occur, due to a recessive gene mutation on a nonsex chromosome. As with CAH, the condition
depends on inherited genetic factors, but the recessive gene must occur on
both the father's and the mother's chromosome in order for 5-Alpha Reductase Deficiency (5-ARD) Syndrome to occur—and when it does, it only
affects male, not female, fetuses. As in two previously described syndromes,
it results in partially to completely feminized external genitals in an XY male
infant, even though the internal reproductive anatomy, including gonads,
is that of a standard XY male. However, it is unique in one respect: when
puberty arrives, the renewed surge of testosterone (for reasons that are still
unclear) is enough to make the previously internalized testes descend, and
the penis to enlarge, and its underlying seam to be completed. Typically male
secondary sex characteristics also appear, and sperm production begins.
There have been a few 5-ARD cases identified in North America, and
slightly more have come from countries in the Middle East and the Far
East. But the largest frequency is found in an isolated village located in the
Dominican Republic, where about one in 90 males are affected. These males
are almost always raised as girls. But with the arrival of almost complete
masculinization in puberty (the locals sometimes call it "Penis-at-Twelve"),
they must decide whether to switch to a male identity. Most of them do, are
accepted as such, and many go on to marry and father children.